window.onload = () => {
    let progress = window.localStorage.getItem('progress');

    if (progress === null) {
        clearProgress();
    }
    else {
        progress = JSON.parse(progress);
    }

    function clearProgress(){
        progress = {
            'perks': new Array(106).fill(0),
            'spells': new Array(393).fill(0),
            'enemies': new Array(171).fill(0),
        }
        saveProgress();
    }

    function saveProgress(){
        window.localStorage.setItem('progress', JSON.stringify(progress));
    }

    function toggleItem(event){
        event.srcElement.className = (event.srcElement.className == 'done item' ? 'item' : 'done item')
        let parts = event.srcElement.style.backgroundImage.split('/');
        progress[parts[2]][parts[3].split('.')[0]] = !progress[parts[2]][parts[3].split('.')[0]];
        saveProgress();
    }

    let tablesProgress = document.querySelector('.progress').childNodes;
    tablesProgress.forEach(table => {
        progress[table.classList[2]].forEach((e, i) => {
            var newItem = document.createElement('div');
            newItem.style.backgroundImage = `url(./imgs/${table.classList[2]}/${i}.png)`;
            if (e == true) {
                newItem.className = 'done item';
            } else newItem.className = 'item';
            table.appendChild(newItem);
            newItem.addEventListener('click', toggleItem);
        });
    });
};